<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::post('/getIp', [ 'uses' => 'IndexController@getIp' ]);
Route::post('/setIp', [ 'uses' => 'IndexController@setIp' ]);
Route::post('/getTableData', [ 'uses' => 'IndexController@getTableData' ]);
Route::post('/loadBigData', [ 'uses' => 'IndexController@loadBigData' ]);
Route::post('/getGraphData', [ 'uses' => 'IndexController@getGraphData' ]);
