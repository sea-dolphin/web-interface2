<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Текущий IP
     * 
     * Вызов команд Linux, возвращает строку.
     * Данный пример пишу под Windows поэтому вместо применения данного оператора просто присваиваю переменной значения
     * которые бы возвратились данной функцией 
     * 
     * @return \Illuminate\Http\Response Ответ в формате JSON
     */
    public function getIp()
    {
        //$strResult = shell_exec('wget -qO- eth0.me');
        $strResult = "31.128.74.150";
        
        $aData = [
            'ip' => $strResult,
            'message_title' => '',
            'message_content' => ''
        ];
        
        return [ 'status' => true, 'data' => $aData ];
    }
    
    /**
     * Установить IP
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response Ответ в формате JSON
     */
    public function setIp(Request $request)
    {
        if ( !empty($request->input('ip')) )
        {
            $aData = [
                'message_title' => 'Success',
                'message_content' => 'IP set seccess'
            ];
            
            return [ 'status' => true, 'data' => $aData ];
        }
        else
        {
            $aData = [
                'message_title' => 'Error',
                'message_content' => 'IP set Error. Detail: ...'
            ];
            
            return [ 'status' => false, 'data' => $aData ];
        }
    }
    
    /**
     * Данные для таблицы
     * 
     * @return \Illuminate\Http\Response Ответ в формате JSON
     */
    public function getTableData()
    {
        $aData = [
            [
                rand(100, 999), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1000, 9999), 
                'Stratum_'.rand(10, 99), 
                'Type_'.rand(10, 99), 
                'When_'.rand(10, 99), 
                'Poll_'.rand(10, 99), 
                'Reach_'.rand(10, 99), 
                'Delay_'.rand(10, 99), 
                'Offset_'.rand(10, 99), 
                'Jitter_'.rand(10, 99)
            ],
            [
                rand(100, 999), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1000, 9999), 
                'Stratum_'.rand(10, 99), 
                'Type_'.rand(10, 99), 
                'When_'.rand(10, 99), 
                'Poll_'.rand(10, 99), 
                'Reach_'.rand(10, 99), 
                'Delay_'.rand(10, 99), 
                'Offset_'.rand(10, 99), 
                'Jitter_'.rand(10, 99)
            ],
            [
                rand(100, 999), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1000, 9999), 
                'Stratum_'.rand(10, 99), 
                'Type_'.rand(10, 99), 
                'When_'.rand(10, 99), 
                'Poll_'.rand(10, 99), 
                'Reach_'.rand(10, 99), 
                'Delay_'.rand(10, 99), 
                'Offset_'.rand(10, 99), 
                'Jitter_'.rand(10, 99)
            ],
            [
                rand(100, 999), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1000, 9999), 
                'Stratum_'.rand(10, 99), 
                'Type_'.rand(10, 99), 
                'When_'.rand(10, 99), 
                'Poll_'.rand(10, 99), 
                'Reach_'.rand(10, 99), 
                'Delay_'.rand(10, 99), 
                'Offset_'.rand(10, 99), 
                'Jitter_'.rand(10, 99)
            ],
            [
                rand(100, 999), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1000, 9999), 
                'Stratum_'.rand(10, 99), 
                'Type_'.rand(10, 99), 
                'When_'.rand(10, 99), 
                'Poll_'.rand(10, 99), 
                'Reach_'.rand(10, 99), 
                'Delay_'.rand(10, 99), 
                'Offset_'.rand(10, 99), 
                'Jitter_'.rand(10, 99)
            ],
            [
                rand(100, 999), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1000, 9999), 
                'Stratum_'.rand(10, 99), 
                'Type_'.rand(10, 99), 
                'When_'.rand(10, 99), 
                'Poll_'.rand(10, 99), 
                'Reach_'.rand(10, 99), 
                'Delay_'.rand(10, 99), 
                'Offset_'.rand(10, 99), 
                'Jitter_'.rand(10, 99)
            ],
            [
                rand(100, 999), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250).'.'.rand(1, 250), 
                rand(1000, 9999), 
                'Stratum_'.rand(10, 99), 
                'Type_'.rand(10, 99), 
                'When_'.rand(10, 99), 
                'Poll_'.rand(10, 99), 
                'Reach_'.rand(10, 99), 
                'Delay_'.rand(10, 99), 
                'Offset_'.rand(10, 99), 
                'Jitter_'.rand(10, 99)
            ],
        ];
        
        return [ 'status' => true, 'data' => $aData ];
    }
    
    /**
     * Загрузка данных
     * 
     * Имитация загрузки больших данных (либо других данных, которые долго формируются) с задержкой 3 скунды
     * 
     * @return \Illuminate\Http\Response Ответ в формате JSON
     */
    public function loadBigData()
    {
        sleep(3);
        $aData = [];
        
        for ($i = 0; $i < 200; $i++)
        {
            $aData[] = rand(100, 99999);
        }
        
        return [ 'status' => true, 'data' => $aData ];
    }
    
    /**
     * Данные для графика
     * 
     * 
     * @return \Illuminate\Http\Response Ответ в формате JSON
     */
    public function getGraphData()
    {
        $aData = [
            [rand(10, 99), rand(10, 99), rand(10, 99), rand(10, 99), rand(10, 99), rand(10, 99), rand(10, 99)],
            [rand(10, 99), rand(10, 99), rand(10, 99), rand(10, 99), rand(10, 99), rand(10, 99), rand(10, 99)]
        ];
        
        return [ 'status' => true, 'data' => $aData ];
    }
}
