myApp.controller('NtpController', 
    function NtpController($scope, $http) {
        
        $scope.stratum = [1, 2, 3, 4, 5, 6, 7 ,8, 9, 10];
        $scope.trusttime = 0;
        $scope.stratum_def = 'Days';
        $scope.period = [ 'Days', 'Hours', 'Minutes', 'Seconds' ];
        
        $scope.tableData = {};
        
        $scope.getTableData = function () {
            $http.post('/getTableData')
                .then(function (response) {
                    console.log(response.data);
                    $scope.tableData = response.data.data;
                });
        };
        
        $scope.getTableData();
    }
)

